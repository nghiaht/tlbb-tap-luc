Hào Hiệp Ấn
===========

* Là trang bị mới ra mắt trong phiên bản trò chơi Quân Lâm Thiên Hạ.
* Dùng 100 điểm chiến công đổi tại NPC Bang hội Triệu Tử Huân.
  Điểm chiến công có được thông qua 2 cách chính:

  * Tham gia hoạt động Hùng Bá Thiên Hạ vào tối T2, T6 hằng tuần.
  * Diệt BOSS cuối các phụ bản sau và nộp vật phẩm Chiến Hồn Ngọc
    (10 chiến công/1, tối đa 200 chiến công/tuần (20 Chiến Hồn Ngọc) tại NPC Triệu Minh Khởi. Nộp CHN trong tuần này, tuần sau nhận chiến công.

    * Yến Tử Ổ (BOSS Mộ Dung Phục)
    * Thiếu Thất Sơn (BOSS Đinh Xuân Thu)
    * Tứ Tuyệt Trang (BOSS Bàng Xí)
    * Binh Thánh Kỳ Trận (BOSS Gia Luật Liên Thành)
    * Huyết Chiến Nhạn Môn Quan (BOSS Gia Luật Hồng Cơ)

* Chia làm 4 loại với 2 thuộc tính chủ đạo: tăng thuộc tính, giảm kháng thuộc tính (4 loại: Băng, Huyền, Hỏa Độc):

  * Thanh Long Hào Hiệp Ấn (Độc Công)
  * Bạch Hổ Hào Hiệp Ấn (Huyền Công)
  * Chu Tước Hào Hiệp Ấn (Hỏa Công)
  * Huyền Vũ Hào Hiệp Ấn (Băng Công)

* Có đánh giá theo cấp trưởng thành, cấp tối đa là 100, cấp Ấn càng cao thì thuộc tính càng cao. Đây được cho là trang bị có thuộc tính cao nhất trò chơi (hơn cả Trùng Lâu).

  * Để tăng độ trưởng thành, dùng vật phẩm **Hào Hiệp Huy Hiệu** (mua trong Tiệm chiến công Bang Hội).
* Ngoài ra còn có các hiệu quả giảm khống chế (Phong huyệt, thất minh, tản công, tê liệt,...)

  * Để mở các hiệu quả này cần dùng điểm công trạng, để có điểm công trạng có thể dùng vật phẩm **Ngự Tứ Lệnh Thưởng** (mua trong Tiệm chiến công Bang Hội).

* Video hướng dẫn cách đổi Hào hiệp ấn tương đối đơn giản cho người cô dơn: https://www.youtube.com/watch?v=aRpCusCLtxs

Mốc cấp trưởng thành của Hào Hiệp Ấn
------------------------------------

.. csv-table:: Mốc cấp trưởng thành của Hào Hiệp Ấn
   :header: "Cấp", "Tên gọi"

   1, Thái Nhất
   10, Lưỡng Nghi
   25, Tam Tài
   40, Tứ Tượng
   55, Ngũ Hành
   70, Lục Hợp
   85, Thất Tinh
   100, Bát Cực