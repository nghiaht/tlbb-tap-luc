Trùng Lâu Triệu
===============

* Tỉ lệ gây Phong Huyệt đối phương, kéo dài 7 giây

  .. figure:: /tlbb_static/hinh_anh/trang_bi/trung_lau/trung_lau_trieu.jpg
     :alt: Bảo vật Ma tôn Trùng Lâu Triệu, có khả năng gây phong huyệt mục tiêu trong 7 giây

     Cùng loại nhẫn (giới chỉ) như :ref:`trang-bi-trung-lau-gioi` nhưng hiệu ứng có suy giảm đôi chút, bảo vật Ma tôn Trùng Lâu Triệu, có khả năng gây phong huyệt mục tiêu trong 7 giây.

  .. figure:: /tlbb_static/hinh_anh/trang_bi/trung_lau/hieu_ung_trung_lau_trieu.png
     :alt: Hình ảnh buff hiệu ứng của thần khí Ma Tôn Trùng lâu Triệu

     Hình ảnh buff hiệu ứng của Trùng lâu Triệu, có xác suất gây phong huyệt mục tiêu trong 7 giây.