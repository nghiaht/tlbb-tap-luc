.. _thoi-trang-phoi-suc-toai-toan-tinh-than:

Toái Toàn Tinh Thần
===================

Xuất hiện trong sự kiện `Thiên Long Thưởng Nguyệt <http://ttl3d.zing.vn/su-kien/thien-long-thuong-nguyet/huong-dan.html>`_ (25/9/2015).

.. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/phoi_suc/toai_toan_tinh_than/toai_toan_tinh_than_khien.jpg

.. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/phoi_suc/toai_toan_tinh_than/toai_toan_tinh_than_yeu.jpg

.. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/phoi_suc/toai_toan_tinh_than/toai_toan_tinh_than_cuoc.jpg