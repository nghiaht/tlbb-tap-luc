.. _lam-dep-thoi-trang:

Thời Trang
==========

.. toctree::

   phi_hong_cam_trang
   bang_tam_nhu_mong
   van_noan_phuong_hoa
   to_van_huyen_thuong
   tu_phong_diep_anh_trung_quang_dao_dat
   luc_da_tien_tung
   giang_sinh_tung_bung
   giang_sinh_long_phung_trinh_tuong
   cam_tu_nien_hoa
   thuong_vu_thai_lam
   da_cam_thien_lan
   bich_huy_ham_phuong
   bich_thao_thanh_tam

Chức Năng
---------

.. toctree::
   
   _nhuom_mau
   phoi_suc/_phoi_suc
