.. tlbb-tap-luc documentation master file, created by
   sphinx-quickstart on Mon Jul 06 23:04:29 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Thiên Long Bát Bộ Cẩm Nang Tạp Lục!
===================================

Những ghi chép và hướng dẫn nho nhỏ trong quá trình tìm hiểu Tân Thiên Long Bát Bộ (TTL, TLBB).

* Tác giả: Võ Đang LingYun - Nguồn gốc máy chủ: Cửu Dương Thần Công - Giao Long 14 - Huyết Lĩnh - Giao Long 7 - Thanh Long.
* Kênh Youtube: `youtube.com/click2teen <http://youtube.com/click2teen>`_
* Fanpage TLBB Cẩm Nang Tạp Lục: `fb.com/chiase.tlbb <http://fb.com/chiase.tlbb>`_
* Facebook đại diện: `facebook.com/profile.php?id=100012802024446 <http://facebook.com/profile.php?id=100012802024446>`_

* Thông tin hình ảnh thời trang, phối sức có thể xem ở mục "Làm đẹp".
* Thông tin hoạt động, sự kiện, phó bản có thể xem ở mục "Hoạt động sự kiện".

.. toctree::
   :maxdepth: 3

   tlbb_tap_luc/nhan_vat/nhan_vat
   tlbb_tap_luc/trang_bi/trang_bi
   tlbb_tap_luc/ky_nang_song/_ky_nang_song
   tlbb_tap_luc/hoat_dong_su_kien/hoat_dong_su_kien
   tlbb_tap_luc/tran_thu/_tran_thu
   tlbb_tap_luc/phien_ban/_phien_ban
   tlbb_tap_luc/thiet_lap/thiet_lap
   tlbb_tap_luc/thong_tin/_thong_tin


Linh tinh
=========

* :ref:`search`
* :download:`Sitemap <sitemap.xml>`
