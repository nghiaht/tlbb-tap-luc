.. _danh-hieu-tinh-thanh-mai-khoi-tien-tu:

Tình Thánh - Mai Khôi Tiên Tử
=============================

Danh hiệu này có được khi một người chơi sử dụng vật phẩm *999 Đóa Hồng* và tặng cho một người chơi khác giới. Danh hiệu có hình bông hoa hồng ở hai đầu hàng chữ.

Cả hai người sẽ nhận được danh hiệu tương ứng và cặp thời trang không khóa, hạn sử dụng trong 90 ngày.

* Người Nam sẽ nhận được danh hiệu *Tình Thánh* và thời trang :ref:`thoi-trang-tu-phong-diep-anh`.

* Người Nữ sẽ nhận được danh hiệu *Mai Khôi Tiên Tử* và thời trang :ref:`thoi-trang-trung-quang-dao-dat`.