Băng Tâm Như Mộng
=================

* Ra mắt sau đợt bảo trì `24/5/2016 <http://ttl3d.zing.vn/su-kien/vao-ha/bang-tam-nhu-mong.html>`_.

* Nhìn lại thời gian gần đây, chúng ta có thể thấy rằng nhà phát hành cứ cách đều 1 tháng ra mắt một thời trang mới, tính đến nay con số đã là 3. Đây có thể là động thái nhằm đa dạng hóa cung-cầu, tăng nguồn thu từ các tín đồ thời trang :)

.. figure:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/bang_tam_nhu_mong/vat_pham.*
   :alt:  Hình ảnh vật phẩm thời trang Băng Tâm Như Mộng bày bán trong tiệm Kim Nguyên Bảo Game Tân Thiên Long

   Hình ảnh vật phẩm thời trang Băng Tâm Như Mộng trong tiệm KNB

.. figure:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/bang_tam_nhu_mong/anh_cap_doi_1.*
   :alt:  Hình ảnh cặp đôi diện thời trang Băng Tâm Như Mộng
   

.. figure:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/bang_tam_nhu_mong/anh_cap_doi_2.*
   :alt:  Hình ảnh cặp đôi diện thời trang Băng Tâm Như Mộng

   Hình ảnh cặp đôi diện thời trang Băng Tâm Như Mộng
