==================
Long Văn Thăng Cấp
==================

---------------
Cấp độ Long Văn
---------------
Vật phẩm: Long Văn.
Dùng Long Văn bất kỳ khác làm nguyên liệu để thăng cấp Long Văn chính.

========== ============ =========
Khoảng cấp Số lượng/cấp Tổng cộng
========== ============ =========
1 - 10     1            10
11 - 20    2            20
21 - 30    3            30
31 - 40    5            50
41 - 50    8            80
51 - 60    13           130
61 - 70    21           210
71 - 80    34           340
81 - 90    55           550
91 - 100   89           890
========== ============ =========


------------------------------
Thuộc tính phát triển Long Văn
------------------------------
Vật phẩm: Xuyết Long Thạch.

Gồm:

* Giới hạn sinh lực tối đa - Dùng Xuyết Long Thạch Nguyên
* Thuộc tính giảm kháng âm - Dùng Xuyết Long Thạch Bạo
* Thuộc tính tấn công - Dùng Xuyết Long Thạch Thương

=== =========================
Cấp Số lượng Xuyết Long Thạch
=== =========================
1   10
2   20
3   30
4   50
5   70
6   90
7   120
8   150
9   180
10  200
=== =========================

----------------
Cấp Sao Long Văn
----------------

Vật phẩm: Ngọc Long Tủy.
Mặc định: 1 sao.
Tối đa: 9 sao.

=== =================================================
Cấp Số lượng Ngọc Long Tủy (NLT), Câu Thiên Thái(CTT)
=== =================================================
2   30
3   60
4   120
5   240
6   400 NLT, 30 CTT Sơ Cấp
7   400 NLT, 60 CTT Trung Cấp
8   400 NLT, 120 CTT Cao Cấp
9   600 NLT, 300 Huyễn Sắc CTT
=== =================================================

| Câu Thiên Thái cấp thấp có thể được thay thế bởi Câu Thiên Thái cấp cao hơn.
| 1 Huyễn Sắc CTT được đổi bằng 5 CTT Cao Cấp.

* Ghi chép: LingYun
* Ngày giờ: 07/07/2015
* Tham khảo, nguồn, trích dẫn: `Long Văn Siêu Cấp - Khẳng Định Đẳng Cấp <http://fgt.vnexpress.net/showthread.php?t=986707>`_
