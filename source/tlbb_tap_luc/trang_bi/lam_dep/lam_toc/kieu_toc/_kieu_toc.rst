Kiểu Tóc
========

* Đến NPC Yến Như Tuyết - Lạc Dương (348, 270).
  
  Chọn kiểu tóc mong muốn (xem trước), muốn thay đổi phải sở hữu vật phẩm kiểu tóc tương ứng (mua trong cửa hàng KNB, rớt từ quái vật các bản đồ luyện cấp, tham gia sự kiện).

  .. figure:: /tlbb_static/hinh_anh/trang_bi/lam_dep/lam_toc/kieu_toc/thay_doi_kieu_toc.png
     :alt: Thay đổi kiểu tóc cần có vật phẩm mẫu kiểu tóc tương ứng

     Kiểu tóc cao quý hiếm có được từ sự kiện Đại Hội Thiên Long, theo ý kiến chủ quan thì chỉ hợp với nhân vật nữ.


Một Số Kiểu Tóc Mới
-------------------

* `Kiểu tóc Hào Hoa <http://ttl3d.zing.vn/su-kien/that-tich/thoi-trang-moi-795.html>`_

.. toctree::   

   van_truy
   ung_dung
