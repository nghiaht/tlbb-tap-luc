Tố Vãn Huyền Thường
===================

* Ra mắt sau đợt bảo trì `30/3/2016 <http://ttl3d.zing.vn/su-kien/du-man-can-khon-moi/to-van-huyen-thuong.html>`_.

* Theo đánh giá cảm quan thì thời trang này nữ mặc nhìn rất thích, nền nã, chân váy áo xúng xính rất truyền thống.

.. figure:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/to_van_huyen_thuong/vat_pham.*
   :alt:  Hình ảnh vật phẩm thời trang Tố Vãn Huyền Thường bày bán trong tiệm Kim Nguyên Bảo Game Tân Thiên Long

   Hình ảnh vật phẩm thời trang Tố Vãn Huyền Thường trong tiệm KNB

.. figure:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/to_van_huyen_thuong/cap_doi_1.*
   :alt:  Hình ảnh cặp đôi diện thời trang Tố Vãn Huyền Thường như Tân lang - Tân giai nhân

   Hình ảnh cặp đôi diện thời trang Tố Vãn Huyền Thường trông như Tân lang - Tân giai nhân

.. figure:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/to_van_huyen_thuong/cap_doi_2.*
   :alt:  Hình ảnh thời trang Tố Vãn Huyền Thường của 2 người chơi

   Hình ảnh thời trang Tố Vãn Huyền Thường của 2 người chơi

.. figure:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/to_van_huyen_thuong/anh_nhan_vat_nu.*
   :alt:  Hình ảnh thời trang Tố Vãn Huyền Thường của một nhân vật nữ khác

   Hình ảnh của một nhân vật nữ khác



