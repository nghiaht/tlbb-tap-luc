Dung Mạo
========

Thay đổi hình thái khuôn mặt của nhân vật.

* Đến NPC Nhan Như Ngọc - Lạc Dương (350, 270).

* Tiêu tốn Định Nhan Châu (Cửa hàng KNB).

  .. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/dung_mao/sua_doi_khuon_mat.png

Biểu Tượng Nhân Vật
-------------------

Thay đổi hình đại diện avatar của nhân vật.

* Đến NPC Nhan Như Ngọc - Lạc Dương (350, 270).

* Tiêu tốn Định Nhan Châu (Cửa hàng KNB).

  .. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/dung_mao/sua_doi_dien_mao_hinh_dai_dien_avatar_ingame.png