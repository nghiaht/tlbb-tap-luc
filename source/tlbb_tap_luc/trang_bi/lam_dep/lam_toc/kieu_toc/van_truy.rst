Vân Truy
========

* Ra mắt sau đợt bảo trì `24/5/2016 <http://ttl3d.zing.vn/su-kien/vao-ha/kieu-toc-moi-van-truy.html>`_.

.. figure:: /tlbb_static/hinh_anh/trang_bi/lam_dep/lam_toc/kieu_toc/van_truy/vat_pham.*
   :alt:  Hình ảnh vật phẩm kiểu tóc Vân Truy bày bán trong tiệm Kim Nguyên Bảo Game Tân Thiên Long

   Hình ảnh vật phẩm kiểu tóc Vân Truy trong tiệm KNB

.. figure:: /tlbb_static/hinh_anh/trang_bi/lam_dep/lam_toc/kieu_toc/van_truy/anh_nhan_vat_nam.*
   :alt:  Hình ảnh mẫu nhân vật nam đổi kiểu tóc Ung Dung

   Hình ảnh mẫu nhân vật nam đổi kiểu tóc Ung Dung

.. figure:: /tlbb_static/hinh_anh/trang_bi/lam_dep/lam_toc/kieu_toc/van_truy/anh_nhan_vat_nu.*
   :alt:  Hình ảnh mẫu nhân vật nữ đổi kiểu tóc Ung Dung

   Hình ảnh mẫu nhân vật nữ đổi kiểu tóc Ung Dung