Ung Dung
========

* Ra mắt sau đợt bảo trì `27/1/2016 <http://ttl3d.zing.vn/su-kien/vu-khuc-mua-xuan/toc-moi-don-xuan.html>`_ (Tóc Mới Đón Xuân).

.. figure:: /tlbb_static/hinh_anh/trang_bi/lam_dep/lam_toc/kieu_toc/ung_dung/vat_pham.*
   :alt:  Hình ảnh vật phẩm kiểu tóc Ung Dung bày bán trong tiệm Kim Nguyên Bảo Game Tân Thiên Long

   Hình ảnh vật phẩm kiểu tóc Ung Dung trong tiệm KNB

.. figure:: /tlbb_static/hinh_anh/trang_bi/lam_dep/lam_toc/kieu_toc/ung_dung/anh_mau_nhan_vat_nam.*
   :alt:  Hình ảnh mẫu nhân vật nam đổi kiểu tóc Ung Dung

   Hình ảnh mẫu nhân vật nam đổi kiểu tóc Ung Dung

.. figure:: /tlbb_static/hinh_anh/trang_bi/lam_dep/lam_toc/kieu_toc/ung_dung/anh_mau_cap_doi_nhan_vat_nam_nu.*
   :alt:  Hình ảnh mẫu nhân vật nam, nữ đổi kiểu tóc Ung Dung

   Hình ảnh mẫu nhân vật nam, nữ đổi kiểu tóc Ung Dung