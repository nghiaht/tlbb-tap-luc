.. _trang-bi-lam-dep:

Làm Đẹp
=======

Hình tượng nhân vật.

.. toctree::   

   thoi_trang/_thoi_trang
   lam_toc/_lam_toc
   dung_mao/_dung_mao


.. sidebar:: Làm đẹp
   :subtitle: Đa dạng & phong phú

   Bên cạnh thời trang, phối sức điểm xuyết, còn có thay đổi dung mạo, tạo mẫu và nhuộm màu tóc.
