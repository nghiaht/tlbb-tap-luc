.. _lam-dep-thoi-trang-phoi-suc:

Điểm Xuyết Phối Sức
===================

Điểm xuyết phối sức vào thời trang tạo vẻ nhìn lung linh, huyền ảo, cá tính hơn khi nhân vật di chuyển trong trò chơi.

Có 3 loại vị trí phối sức khác nhau áp dụng vào cơ thể người mặc, phối sức cấp càng cao thì hiệu ứng tỏa ra càng rực rỡ, đậm đà.

* Khiên - phần vai.
* Yêu - phần thân.
* Cước - phần chân.


Cách điểm xuyết phối sức vào thời trang cũng *tương tự* như khảm bảo thạch vào trang bị. Tại NPC Y Thiên Thái - Lạc Dương (345, 269):

* Dùng Kim Thoa (25 KNB) để cắt sửa (đục lỗ) thời trang, tối đa 3 lỗ.
  
  .. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/phoi_suc/kim_thoa.png

  .. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/phoi_suc/cat_sua_thoi_trang.png

* Thời Trang Phối Sức Điểm Xuyết phù (120 KNB).
  
  .. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/phoi_suc/thoi_trang_phoi_suc_diem_xuyet_phu.png

* Cùng với phối sức đang sở hữu, tiến hành điểm xuyết, có thể *xem trước* hiệu ứng phối sức áp dụng lên thời trang. Chú ý

  .. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/phoi_suc/diem_xuyet_trang_suc_thoi_trang.png

* | Có thể tháo phối sức ra nếu không ưng ý, dùng Thời Trang Phối Sức Thanh Trừ phù (30 KNB).
  | Tốn 145 KNB cho một lần khảm phối sức, vì vậy hãy cân nhắc kỹ chi tiêu.

Nâng Cấp Phối Sức
-----------------

Có thể nâng cấp phối sức bằng cách hợp thành 5 phối sức cùng cấp, Thời Trang Phối Sức Gia Công phù (70 KNB).

  .. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/phoi_suc/thoi_trang_phoi_suc_gia_cong_phu.png

  .. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/phoi_suc/gia_cong_trang_suc_thoi_trang.png



Danh Sách Phối Sức
------------------

.. toctree::   
   
   dieu_vu_phuong_lan
   toai_toan_tinh_than