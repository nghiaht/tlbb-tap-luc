.. _ky-nang-song-duoc:

Nghề Dược
=========

Học kỹ năng hái dược, chế dược, dưỡng sinh học tại khu Dược Phố - Lạc Dương (229, 309), hoặc tại Đại Lý, Tô Châu, thành thị bang hội.

Hái Dược
--------

Khi hái dược phải mang theo *liềm hái thuốc*. Tiêu hao tinh lực khi hái dược.

.. csv-table:: Bảng phân bố vị trí hái nguyên vật liệu thảo dược
   :header: "Cấp", "Dược phẩm 1", "Dược phẩm 2", "Dược phẩm 3", "Vị trí"
   :widths: 5, 10, 10, 10, 15

   1, "Bạch Anh", "Bồ Hoàng",, "Vô Lượng Sơn, Kiếm Các, Đôn Hoàng, Kính Hồ, Tung Sơn, Thái Hồ"
   2, "Xuyên bối", "Nguyên hồ",, "Tây Hồ"
   3, "Tỳ bà", "Cam thảo", "Kim ngân hoa", "Nhĩ Hải"
   4, "Hoàng cầm", "Câu kỷ", "Trầm hương", "Nhạn Nam"
   5, "Đỗ trọng", "Thương thuật", "Phục linh", "Long Tuyền"
   6, "Phòng phong", "Hương nhu", "Hoàng Liên", "Thương Sơn"
   7, "Đương qui", "Quế tâm", "Hương phụ", "Nhạn Bắc"
   8, "Hoắc hương", "Hồi thần thảo", "Thủ ô", "Võ Di"
   9, "Đông trùng hạ thảo", "Long quí tử",, "Thạch Lâm"
   10, "Tượng bối", "Nhân sâm",, "Thảo Nguyên"
   10, "Linh chi", "Tuần thảo",, "Mai Lĩnh"
   10, "Hạt sen", "Khô mộc xuân",, "Ngọc Khê, Liêu Tây, Nam Vực, Nam Chiêu, Trường Bạch Sơn, Quỳnh Châu, Miêu Cương, Hoàng Long Phủ"

Tham khảo: Hướng dẫn sẵn có trong trò chơi Thiên Long Bát Bộ.
   

Chế Dược
--------

Thực hiện chế dược khi có đủ dược thảo theo công thức, tiêu hao hoạt lực và phải đứng gần lò luyện đan.

Công thức chế tạo dược phẩm cấp cao có thể mua trong công trình nghiên cứu của thành thị bang hội.

.. image:: /tlbb_static/hinh_anh/ky_nang_song/duoc/che_duoc_gan_lo_luyen_dan.png
