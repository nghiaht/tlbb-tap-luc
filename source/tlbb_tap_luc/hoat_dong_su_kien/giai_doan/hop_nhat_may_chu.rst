Hợp Nhất Máy Chủ
================

Hợp nhất máy chủ Thiên Long Bát Bộ (TTLBB), Tân Thiên Long hay còn được gọi bằng câu cửa miệng *hợp long*, hợp server nhằm tháo gỡ tình trạng người chơi giảm sút.

Các máy chủ sau khi sát nhập sẽ nảy sinh các cuộc tranh chiến, duy trì lượng người chơi với máu PK dài dài.

*CẬP NHẬT 2018* - Các bạn có thể sử dụng `web tool này để tra cứu trực tiếp lịch sử, tên máy chủ cũ, mới mà mình muốn <https://nghiaht.github.io/tlbb-web-toolkit/>`_

Bên dưới là thông tin các đợt hợp nhất máy chủ kể từ lúc NPH VNG vận hành game Tân Thiên Long, sắp xếp theo thứ tự gần đây nhất trở về trước.

   1. `10/1/2018 - 31/1/2018 <http://ttl3d.zing.vn/su-kien/thong-nhat-thien-ha/noi-dung-chinh-1316.html>`_

     **ĐẠI THẾ GIỚI I**

     * Thanh Long + Giao Long 7 -> Thanh Long (10/1/2018)
     * Yến Vương + Thông Thiên -> Huyền Tố (17/1/2018)
     * Bạch Hổ
     * Thánh Hỏa

     **ĐẠI THẾ GIỚI II**

     * (Thiên Long 12 + Thiên Long 13) + Thiên Long 11 -> Thiên Long 11 (24/1/2018)
     * (Thiên Long 15 + Thiên Long 16) + Thiên Long 14 -> Thiên Long 14 (31/1/2018)
     * Thiên Long 01

     Thông tin thêm: Đại Thế Giới III cũ (Chu Tước, TLCV..) bị âm thầm khai tử vì NPH vẫn chưa giải quyết thỏa đáng tình trạng hack KNB diễn ra ở vùng đất này.


   3. `23/11 - 14/12/2016 <http://ttl3d.zing.vn/su-kien/hop-nhat-may-chu-11-2016/gioi-thieu-872.html>`_

      **ĐẠI THẾ GIỚI I**

      * (Long Vũ Môn + Hàn Băng Miên Chưởng) + Hỏa Diệm -> Thanh Long
      * Giao Long 7
      * Thiên Long 1

      **ĐẠI THẾ GIỚI II**

      * Tuyết Nguyên + Giao Long 8 -> Bạch Hổ
      * Thánh Hỏa
      * Thông Thiên
      * Yến Vương

      **ĐẠI THẾ GIỚI III**

      * Thiên Long 3 + Thiên Long 2 -> Chu Tước
      * (Thiên Long 9 + Thiên Long 10) + Thiên Long 7 -> Huyền Vũ
      * Thiên Long Chuyển Vận

      **ĐẠI THẾ GIỚI IV**

      * Thiên Long 11,12,13

      **ĐẠI THẾ GIỚI V**

      * Thiên Long 14

   2. `6/1/2016 - 20/1/2016 <http://ttl3d.zing.vn/su-kien/hop-nhat-may-chu-2016/gioi-thieu-chung-522.html>`_

      **TÂN THẾ GIỚI 3D - I**

      * Thiên Long 1 + Bàn Cổ -> Thiên Long 1
      * Thiên Long 2 + Thiên Long 3 -> Thiên Long 2
      * Thiên Long 8 + Thiên Long 4,5,6-> Thiên Long 3
      * Thiên Long Chuyển Vận

      **TÂN THẾ GIỚI 3D - II** (Thông tin thêm, không có trong đợt hợp nhất)

      * Thiên Long 11,12,13

      **TÂN THẾ GIỚI 3D - III**

      * Thiên Long 14

      **ĐẠI THẾ GIỚI IV**

      * Thiên Long 7,9,10

   1. `15/4/2015 - 13/5/2015 <http://ttl3d.zing.vn/su-kien/hop-nhat-may-chu/gioi-thieu-214.html>`_

      **ĐẠI THẾ GIỚI I**

      * Hoa Sơn + Thánh Hỏa -> Thánh Hỏa
      * Giao Long 10 + Thông Thiên -> Thông Thiên
      * Hàng Long Phục Hổ + Hàn Băng Miên Chưởng -> Hàn Băng Miên Chưởng

      **ĐẠI THẾ GIỚI II**

      * Giao Long 11 + Giao Long 8 -> Giao Long 8
      * Giao Long 9 + Yến Vương -> Yến Vương
      * Côn Lôn + Tuyết Nguyên -> Tuyết Nguyên

      **ĐẠI THẾ GIỚI III**

      * Huyết Lĩnh + Giao Long 7 -> Giao Long 7
      * Giao Long 13 + Hỏa Diệm -> Hỏa Diệm
      * Hoàng Long + Long Vũ Môn -> Long Vũ Môn




