Phi Hồng Cẩm Trang
==================

* Ra mắt sau đợt bảo trì `10/8/2016 <http://ttl3d.zing.vn/su-kien/that-tich/thoi-trang-moi-795.html>`_.

* Theo thông tin từ NPH, thời trang này có 8 màu nhuộm khác nhau, nhìn rất ư là *thiên thần*, song song đó là kiểu tóc **Hào Hoa** và khuôn mặt **Nho Nhã** mà mình đánh giá là rất đẹp.

.. figure:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/phi_hong_cam_trang/vat_pham.*
   :alt:  Hình ảnh vật phẩm thời trang Phi Hồng Cẩm Trang bày bán trong tiệm Kim Nguyên Bảo Game Tân Thiên Long

   Hình ảnh vật phẩm thời trang Phi Hồng Cẩm Trang bày bán trong tiệm Kim Nguyên Bảo Game Tân Thiên Long

.. figure:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/phi_hong_cam_trang/phong_cach_linh_mai.*
   :alt:  Hình ảnh màu nhuộm Linh Mai - rất ư là *thiên thần* của thời trang Phi Hồng Cẩm Trang

   Hình ảnh màu nhuộm Linh Mai - rất ư là *thiên thần* của thời trang Phi Hồng Cẩm Trang
   

.. figure:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/phi_hong_cam_trang/phong_cach_phi_hong.*
   :alt:  Hình ảnh màu nhuộm cơ bản Phi Hồng, sắc hồng cơ bản của thời trang Phi Hồng Cẩm Trang

   Hình ảnh màu nhuộm cơ bản Phi Hồng, sắc hồng cơ bản của thời trang Phi Hồng Cẩm Trang
