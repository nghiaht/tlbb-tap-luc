Khai Khoáng
===========

Khai Khoáng có thể học tại khu lò rèn ở các thành (Lò rèn Đại Lý hoặc Tô Châu Thiết Tượng Phố). 

Khi đi đào khoáng phải mang theo *cuốc khai khoáng*. Tiêu hao tinh lực khi đào khoáng.

.. csv-table:: Bảng phân bố vị trí đào nguyên vật liệu khoáng sản
   :header: "Cấp", "Khoáng sản", "Bạn sinh 1", "Bạn sinh 2", "Bạn sinh 3", "Bạn sinh 4", "Vị trí"
   :widths: 5, 10, 10, 10, 10, 10, 15

   1, "Đồng", "Huy Đồng", "Lưu Đồng", "Bạch Diên", "Lam Đồng", "Vô Lượng Sơn, Kiếm Các, Kính Hồ, Đôn Hoàng, Thái Hồ, Tây Hồ, Tung Sơn"
   2, "Thiết", "Hoàng Thiết", "Hạt Thiết", "Lăng Thiết", "Đoàn Thiết", "Nhĩ Hải"
   3, "Ngân", "Thạch Cao Thạch", "Điển Ngân", "Hắc Ngân", "Bạch Tân", "Nhĩ Hải, Nhạn Nam"
   4, "Hàn Thiết", "Trọng Tinh Thạch", "Vân Mẫu Thạch", "Thạch Anh Thạch", "Đĩnh Thạch", "Nhạn Nam, Long Tuyền"
   5, "Kim", "Phất Thạch", "Hoạt Thạch", "Trường Thạch", "Phương Giải Thạch", "Long Tuyền, Thương Sơn"
   6, "Huyền Thiết", "Phương Ngọc Thạch", "Diệp Lạp Thạch", "Hải Bào Thạch", "Thấu Huy Thạch", "Thương Sơn, Nhạn Bắc"
   7, "Thủy Tinh", "Băng Châu Thạch", "Phương Trụ Thạch", "Y Lợi", "Chất Thạch", "Nhạn Bắc, Võ Di"
   8, "Phỉ Thúy", "Bành Nhuận Thạch", "Lụy Thách Thạch", "Mang Tiêu Thạch", "Huy Lục Thạch", "Võ Di, Thạch Lâm"
   9, "Chấn Vũ", "An Sơn Thạch", "Độc Trọng Thạch", "Cảm Lãm Thạch", "Lam Thạch", "Thạch Lâm, Thảo Nguyên"
   10, "Long Huyết", "Hà Thạch", "Bạch Ác Thạch", "Phù Thạch", "Giác Thiểm", "Thảo Nguyên, Mai Lĩnh, Ngân Ngai Tuyết Nguyên"
   10, "Phụng Huyết",,,,, "Mai Lĩnh, Ngọc Khê"
   10, "Nữ Oa",,,,, "Ngọc Khê, Liêu Tây, Nam Vực, Nam Chiêu, Trường Bạch Sơn, Quỳnh Châu, Miêu Cương, Hoàng Long Phủ"
