.. _ky-nang-song-da-tao-do:

Đả Tạo Đồ - Đồ Tường
====================

* Đả Tạo Đồ: Nguyên liệu mỗi khi chế tạo một trang bị mà kỹ năng sống yêu cầu.
* Đồ Tường: Công thức để học được cách chế tạo một trang bị, tức chỉ cần dùng một lần mà thôi.

Đả tạo đồ được phân cấp 1-10, nhưng chúng ta cứ cho rằng đó là các cấp độ trang bị để nhân vật mặc vào khác nhau như 4x, 5x,.. 9x, 10x.
Việc đối chiếu cấp đả tạo đồ 1-10 với trang bị 1x - 10x cần phải chú ý để tìm mua đả tạo đồ cho đúng.

Hiện tại, quy ước như sau:

* Với các loại vũ khí, đả tạo đồ cấp nào thì chế được vũ khí có mức cấp độ nhân vật ngang bằng cấp đó.

  Ví dụ như Đả tạo đồ vũ khí cấp 5 thì sẽ chế được vũ khí 5x.

* Với các loại đồ phòng thủ may mặc, công nghệ (giới chỉ, hộ phù, hạng liên), đả tạo đồ cấp nào sẽ chế được trang bị có mức cấp nhỏ hơn 1 mức.

  Ví dụ như đả tạo đồ phòng thủ cấp 5 thì sẽ chế được trang bị 4x.

Đả Tạo Đồ
---------

Hướng dẫn cách tìm kiếm đả tạo đồ trong game Tân Thiên Long Bát Bộ (TTLBB3D).

* Cấp 3, 4 (Đả tạo đồ 3x, 4x):

  * Đả tạo đồ phòng thủ (may mặc): NPC May vá Dư Hóa Long - Tô Châu (310, 165).

    .. figure:: /tlbb_static/hinh_anh/ky_nang_song/da_tao_do_tuong/da_tao_do_3_4_nghe_may_mac_npc_du_hoa_long_to_chau_danh_sach.png
       :alt: Đả tạo đồ chế phòng thủ 2x, 3x mua tại NPC May vá Dư Hóa Long - Tô Châu (350, 248)

       Đả tạo đồ chế phòng thủ 2x, 3x mua tại NPC May vá Dư Hóa Long  Tô Châu (350, 248).
  * Đả tạo đồ nghề đúc (vũ khí): NPC Tiết Chúc - Tô Châu (350, 248).

    .. figure:: /tlbb_static/hinh_anh/ky_nang_song/da_tao_do_tuong/da_tao_do_3_4_nghe_duc_npc_tiet_chuc_to_chau_danh_sach.png
       :alt: Đả tạo đồ nghề đúc chế vũ khí 3x, 4x mua tại NPC Tiết Chúc (dạy nghề đúc, luyện kim) - Tô Châu (350, 248)

       Đả tạo đồ nghề đúc chế vũ khí 3x, 4x mua tại NPC Tiết Chúc (dạy nghề đúc, luyện kim) - Tô Châu (350, 248).
* Cấp 5: NPC Lưu Quý - Liêu Tây (279, 48), ngay trước cổng qua Ngân Ngai Tuyết Nguyên.

  .. figure:: /tlbb_static/hinh_anh/ky_nang_song/da_tao_do_tuong/da_tao_do_5_npc_luu_qui_lieu_tay.png
     :alt: NPC Lưu Quý - Liêu Tây (279, 48) đứng ngay trước cổng qua Ngân Ngai Tuyết Nguyên, chuyên bán đả tạo đồ cấp 5

     NPC Lưu Quý - Liêu Tây (279, 48) đứng ngay trước cổng qua Ngân Ngai Tuyết Nguyên, chuyên bán đả tạo đồ cấp 5.
  .. figure:: /tlbb_static/hinh_anh/ky_nang_song/da_tao_do_tuong/da_tao_do_5_npc_luu_qui_lieu_tay_danh_sach.png
     :alt: NPC Lưu Quý - Liêu Tây (279, 48) đứng ngay trước cổng qua Ngân Ngai Tuyết Nguyên, chuyên bán đả tạo đồ cấp 5

     Bán tổng hợp đả tạo đồ cấp 5 gồm vũ khí 5x; đồ chế may mặc phòng thủ, công nghệ 4x (hạng liên, giới chỉ, hộ phù).

* Cấp 6: NPC Võ Cát - Quỳnh Châu (280, 215).
* Dùng điểm thiện ác đổi tại NPC Lý Lạc Thi - Tô Châu (290, 186).

  .. figure:: /tlbb_static/hinh_anh/ky_nang_song/da_tao_do_tuong/da_tao_do_ngau_nhien_npc_ly_lac_thi_to_chau_thien_ac.png
     :alt: Mua tất cả các loại đả tạo đồ bằng điểm thiện ác

     Hằng giờ, NPC này sẽ ngẫu nhiên bán một số loại đả tạo đồ cấp 1-10, giám định phù cấp 1-10, và phải dùng điểm thiện ác để mua.
     Điểm thiện ác có được thông qua việc tổ đội luyện cấp cùng người chơi cấp thấp hơn.

* Mua từ NPC Trại Lỗ Ban - Lạc Dương.

  .. figure:: /tlbb_static/hinh_anh/ky_nang_song/da_tao_do_tuong/da_tao_do_ngau_nhien_npc_trai_lo_ban_lac_duong.png
     :alt: Mua tất cả các loại đả tạo đồ bằng vàng

     Theo chu kỳ, cứ cách mấy mươi phút, NPC này sẽ đứng ở xa phu phía Đông Lạc Dương Cửu Châu Thương Hội hoặc di chuyển trên Đại lộ Đông Tây, và bày bán các loại đả tạo đồ có cấp ngẫu nhiên.

.. _ky-nang-song-do-tuong:

Đồ Tường
--------

Hướng dẫn cách tìm kiếm công thức chế đồ - đồ tường trong game Tân Thiên Long Bát Bộ (TTLBB3D).

* Cấp 1, 2: NPC May vá ở các thành Đại Lý, Lạc Dương, Tô Châu. Ví dụ như NPC May vá Dư Hóa Long - Tô Châu (310, 165).
* Cấp 3, 4: NPC Mã Đầu Lĩnh - Thương Sơn (70, 48).

  .. figure:: /tlbb_static/hinh_anh/ky_nang_song/da_tao_do_tuong/do_tuong_3_4_npc_ma_dau_linh_thuong_son_danh_sach.png
     :alt: Đồ tường cấp 3, 4.

     NPC Mã Đầu Lĩnh - Thương Sơn (70, 48), đứng ở góc trái bên trên cùng bản đồ.
     Đồ tường may mặc và công nghệ cấp 3 (trang bị đồ chế 3x), đúc vũ khí cấp 4 (vũ khí 4x).

* Cấp 4, 5 (Đồ tường 4x): NPC Quách Để Huy - Yến Vương Cổ Mộ Tầng 5 (95, 25), trước cổng qua tầng 6.

  .. figure:: /tlbb_static/hinh_anh/ky_nang_song/da_tao_do_tuong/do_tuong_4_5_npc_quach_de_huy_yvcm5.png
     :alt: Đồ tường cấp 4, 5

     NPC Quách Để Huy - gần vị trí BOSS Phục Địa Ma - Yến Vương Cổ Mộ tầng 5, ngay trước cổng lên tầng 6.
  .. figure:: /tlbb_static/hinh_anh/ky_nang_song/da_tao_do_tuong/do_tuong_4_5_npc_quach_de_huy_yvcm5_danh_sach.png
     :alt: Đồ tường cấp 4, 5

     Bán tất cả đồ tường may mặc và công nghệ cấp 4 (trang bị đồ chế 4x, hộ phù, giới chỉ, hạng liên), đúc cấp 5 (vũ khí 5x).
     Gần vị trí BOSS Phục Địa Ma - Yến Vương Cổ Mộ tầng 5, ngay trước cổng lên tầng 6.

* Mở rương trên các bản đồ cấp tương ứng.
* Mua đồ tường trong thành thị Bang hội, cấp đồ tường tương ứng với cấp kiến trúc liên quan cho phép.
* Dùng độ cống hiến môn phái đổi tại trưởng môn.

  .. image:: /tlbb_static/hinh_anh/ky_nang_song/da_tao_do_tuong/do_tuong_mua_tai_mon_phai_1.png

  .. figure:: /tlbb_static/hinh_anh/ky_nang_song/da_tao_do_tuong/do_tuong_mua_tai_mon_phai_2.png
     :scale: 50%
     :alt: Dùng điểm cống hiến môn phái mua phối phương đồ tường chế tạo

     Phối phương đồ tường chế tạo cấp cao nhất hiện tại - cấp 10 tiêu tốn 130 điểm cống hiến môn phái.
