.. _trang-bi-trung-lau-thu:

Trùng Lâu Thủ
=============

* Tăng sát thương 10% (Trùng Lâu Thủ)
* Tăng sát thương 15% (Chân Trùng Lâu Thủ)
* Ra mắt TTL3D vào ngày 10/06/2015 (Sự kiện Pháo Hoa Trùng Lâu)

  .. figure:: /tlbb_static/hinh_anh/trang_bi/trung_lau/trung_lau_thu.jpg
     :alt: Hình ảnh trang bị Trùng Lâu Thủ

     Hình ảnh Trang bị Trùng Lâu Thủ. Trang bị xét về tầm tác dụng cũng đáng mua giống như :ref:`trang-bi-trung-lau-ngoa`.

  .. figure:: /tlbb_static/hinh_anh/trang_bi/trung_lau/chan_trung_lau_thu.jpg
     :alt: Hình ảnh trang bị Chân Trùng Lâu Thủ

     Hình ảnh trang bị Chân Trùng Lâu Thủ.

* Giá thị trường: 15 triệu, 40 triệu
