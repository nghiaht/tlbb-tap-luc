===================
Tinh Thông Trang Bị
===================

Tinh thông trang bị cho phép mở thêm 3 dòng thuộc tính bổ sung vào hầu hết các trang bị (ngoại trừ Thần khí, Ám Khí, Long văn, Võ hồn, Lệnh bài bang hội).

.. image:: /tlbb_static/hinh_anh/trang_bi/tinh_thong_trang_bi/thuoc_tinh_bo_sung_tu_tinh_thong_trang_bi.png

Thực hiện tất cả thao tác liên quan đến Tinh Thông Trang Bị tại NPC Mộ Bạch - Tô Châu Thiết Tượng Phố (363, 243).

Sở Hữu Thuộc Tính Tinh Thông Trang Bị
-------------------------------------

Để sở hữu, cũng như thay đổi (luyện lại) thuộc tính tinh thông trang bị mới, cần có Ly Hỏa (Luyện Ly hỏa hoặc :ref:`hoa-giai-trang-bi-thu-cong`).

.. image:: /tlbb_static/hinh_anh/trang_bi/tinh_thong_trang_bi/vat_pham_ly_hoa.png

.. figure:: /tlbb_static/hinh_anh/trang_bi/tinh_thong_trang_bi/lo_ly_hoa_kim_tinh_thach_toai_phien.png
   :alt: Luyện ly hỏa và kim tinh thạch toái phiến tinh thông trang bị

   Mỗi ngày được 5 lần luyện Ly hỏa miễn phí tại NPC Mộ Bạch - Tô Châu, sau khi luyện, nhấn nút *Lấy đá* để nhận Ly hỏa và Kim tinh thạch toái phiến.

Sau đó tiến hành tôi luyện thuộc tính tinh thông để tạo thuộc tính mới:

.. image:: /tlbb_static/hinh_anh/trang_bi/tinh_thong_trang_bi/toi_luyen_trang_bi.png


Thăng Cấp Tinh Thông Trang Bị
-----------------------------

.. image:: /tlbb_static/hinh_anh/trang_bi/tinh_thong_trang_bi/vat_pham_manh_vo_kim_tinh_thach_toai_phien.png

.. figure:: /tlbb_static/hinh_anh/trang_bi/tinh_thong_trang_bi/vat_pham_kim_tinh_thach.png
   :alt: Kim tinh thạch tinh thông trang bị

   Kim tinh thạch có được thông qua Luyện ly hỏa, hoặc hoạt động Lôi đài tỳ vỏ (14h Chủ Nhật hằng tuần).


Cứ mỗi 2 Kim tinh thạch toái phiến (thông qua luyện lò Ly hỏa) thì đổi được 1 Kim tinh thạch.

Thực hiện thăng cấp tinh thông trang bị, cấp tinh thông càng cao thì số lượng Kim tinh thạch yêu cầu càng nhiều.

.. image:: /tlbb_static/hinh_anh/trang_bi/tinh_thong_trang_bi/nang_cap_thuoc_tinh_tinh_thong_trang_bi.png


Ghi chép số lượng vật phẩm Kim Tinh Thạch để thăng cấp tinh thông trang bị từ cấp 1 - 100.

===================== ========================= =============
Khoảng cấp tinh thông Số lượng vật phẩm cần/lần Tổng cộng
===================== ========================= =============
 1 - 10                1						10 x 1 = 10
11 - 20                6						 60
21 - 30               12						120
31 - 40               14						140
41 - 50               16						160
51 - 60               18						180
61 - 70               20						200
71 - 80               22						220
81 - 90               24						240
91 - 100              25						250
TC:                                             1580
===================== ========================= =============

Thông tin đã được kiểm chứng từ cấp tinh thông 70 trở xuống. Phần còn lại xin cảm ơn Đoàn_Ca - Diễn đàn Tân Thiên Long.

Di Chuyển Thuộc Tính Tinh Thông
-------------------------------

Muốn di chuyển toàn bộ thuộc tính tinh thông từ trang bị cũ sang một trang bị mới cùng loại (ví dụ như hộ phù -> hộ phù, giới chỉ -> giới chỉ), chỉ cần tiêu tốn ít vàng, thực hiện tại NPC Mộ Bạch.

.. image:: /tlbb_static/hinh_anh/trang_bi/tinh_thong_trang_bi/chuyen_doi_thuoc_tinh_tinh_thong.png

.. _hoa-giai-trang-bi-thu-cong:

Hóa Giải Trang Bị Thủ Công
--------------------------

Các món đồ chế trên 7 sao không dùng đến, thay vì bỏ đi có thể phân giải đổi thành Ly Hỏa.

.. image:: /tlbb_static/hinh_anh/trang_bi/tinh_thong_trang_bi/tach_hoa_giai_trang_bi_thu_cong.png


* Ghi chép: LingYun
* Ngày giờ: 06/07/2015
* Tham khảo, nguồn, trích dẫn: `Bao nhiêu Kim Tinh Thạch thì sẽ lên tinh thông 100 - Khởi xướng: Enel05 - Diễn đàn TTL <http://diendan.zing.vn/showthread.php?t=6419174&p=3818291908&highlight=#post3818291908>`_
