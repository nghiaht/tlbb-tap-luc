.. _thu-cuoi-yen-vu-thanh-ha:

Yên Vũ Thanh Hà
===============

* Xuất hiện trong sự kiện
  `Báo Danh Năng Động <http://ttl3d.zing.vn/su-kien/dung-doat-bao-ruong/bao-danh-nang-dong-488.html>`_ (4/11/2015)
  cùng thời trang :ref:`thoi-trang-thuong-vu-thai-lam`.

* Có thể phi hành, cùng cưỡi tổ đội.

.. image:: /tlbb_static/hinh_anh/trang_bi/thu_cuoi/yen_vu_thanh_ha/hinh_anh.jpg