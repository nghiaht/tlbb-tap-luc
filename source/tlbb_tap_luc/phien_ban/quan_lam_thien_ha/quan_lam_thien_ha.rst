Quân Lâm Thiên Hạ
=================

Offline thông báo bản cập nhật Quân Lâm Thiên Hạ vào ngày 3/7 tại Tp. HCM.

Việc mới, vật mới:

* Trang bị Hào Hiệp Ấn.
* Hoạt động Hùng Bá Thiên Hạ.
* Dị Dung Các (lưu 5 kiểu khuôn mặt, 5 kiểu tóc, 5 màu tóc).
* Càn quét phó bản (tốn 2 điểm càn quét/lần).
* Kim Lan Trận.
* Ngự Thú
* Vận Tiêu
* Huyết chiến Nhạn Môn Quan.


