.. _lam-dep-thoi-trang-nhuom-mau:

Nhuộm Màu
=========

* Chỉ những thời trang hỗ trợ nhuộm màu mới có thể thực hiện thao tác này.

  .. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/nhuom_mau/thoi_trang_co_the_nhuom_mau.png

* | Tiêu tốn 1 Hồng Diệu Thạch (mua trong cửa hàng KNB, Bách bảo các thành thị bang hội, hoặc ngoài thị trường giá ~200 vàng), 5 vàng cho mỗi lần nhuộm màu tại NPC Y Thiên Thái - Lạc Dương (345, 269).
  | Có thể chọn màu nhuộm mong muốn trong danh sách, và nhấn *Tự nhuộm*, trong hành trang phải có kha khá Hồng Diệu Thạch, vàng, hệ thống sẽ tự động tiêu hao vật phẩm nhuộm cho đến khi đạt được màu nhuộm mục tiêu (Hên xui, nếu may mắn thì chỉ cần vài lần là có thể được màu tốt).


  .. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/nhuom_mau/hong_dieu_thach.png

  .. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/nhuom_mau/nhuom_mau.png

* | Có thể tự do xem trước tất cả màu nhuộm, từ bình thường đến cao cấp, hiếm của thời trang mà bạn sở hữu, bằng cách nhấn *Giám* trong giao diện thao tác nhuộm màu. 
  | Nếu ngần ngại, hãy nhờ bạn bè, một người nào đó có sở hữu thời trang mà bạn thích chụp hình các màu nhuộm để tham khảo trước khi quyết định mua thời trang, và nhuộm màu!

  .. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/nhuom_mau/xem_truoc_mau_nhuom.png
